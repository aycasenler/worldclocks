package com.example.ayca.worldclocks.retrofit

import com.example.ayca.worldclocks.model.MainData
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.http.Path

interface ApiInterface{
    @GET("/timezone/{area}/{location}")
    fun getCitys(@Path("area") area: String, @Path("location") location: String) : Call<String>

}