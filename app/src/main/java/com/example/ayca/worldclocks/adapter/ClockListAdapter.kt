package com.example.ayca.worldclocks.adapter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.worldclocks.HomeScreenFragment
import com.example.ayca.worldclocks.R
import com.example.ayca.worldclocks.retrofit.ApiClient
import com.google.gson.Gson
import com.iigo.library.ClockView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("DEPRECATION")
class ClockListAdapter(
    private var clockDetailList: MutableList<HomeScreenFragment.ClockDetail>,
    private var context: Context
) :
    RecyclerView.Adapter<ClockListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.clock_list_item,
                parent,
                false
            )
        )
    }

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {

        var clockViewBlack: ClockView
        var clockViewWhite: ClockView
        var clockViewPink: ClockView
        var clockViewYellow: ClockView
        var clockViewBlue: ClockView
        var clockViewRed: ClockView
        var clockViewGrey: ClockView
        var clockViewOrange: ClockView
        var clockViewPurple: ClockView
        var clockViewGreen: ClockView
        var cityNameTxt: TextView
        var timeOffsetTxt: TextView
        var preferences: SharedPreferences
        var editor: SharedPreferences.Editor
        var clockListForDelete: MutableList<HomeScreenFragment.ClockDetail> = mutableListOf()
        val mainHandler = Handler(Looper.getMainLooper())
        init {
            clockViewBlack = itemLayoutView.findViewById(R.id.clockViewBlack)
            clockViewBlue = itemLayoutView.findViewById(R.id.clockViewBlue)
            clockViewGreen = itemLayoutView.findViewById(R.id.clockViewGreen)
            clockViewGrey = itemLayoutView.findViewById(R.id.clockViewGrey)
            clockViewOrange = itemLayoutView.findViewById(R.id.clockViewOrange)
            clockViewPink = itemLayoutView.findViewById(R.id.clockViewPink)
            clockViewPurple = itemLayoutView.findViewById(R.id.clockViewPurple)
            clockViewWhite = itemLayoutView.findViewById(R.id.clockViewWhite)
            clockViewRed = itemLayoutView.findViewById(R.id.clockViewRed)
            clockViewYellow = itemLayoutView.findViewById(R.id.clockViewYellow)
            cityNameTxt = itemLayoutView.findViewById(R.id.city_name_txt)
            timeOffsetTxt = itemLayoutView.findViewById(R.id.time_offset_txt)
            preferences =
                itemLayoutView.context.getSharedPreferences("clockList", Context.MODE_PRIVATE)
            editor = preferences.edit()
            clockListForDelete = mutableListOf()
        }
    }

    override fun getItemCount(): Int {
        return clockDetailList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val clock = clockDetailList[position]

        when (clock.backgroundColor) {
            "Black" -> {
                holder.clockViewBlack.setBackgroundColor(Color.BLACK)
                holder.clockViewBlue.setBackgroundColor(Color.BLACK)
                holder.clockViewGreen.setBackgroundColor(Color.BLACK)
                holder.clockViewGrey.setBackgroundColor(Color.BLACK)
                holder.clockViewOrange.setBackgroundColor(Color.BLACK)
                holder.clockViewPink.setBackgroundColor(Color.BLACK)
                holder.clockViewPurple.setBackgroundColor(Color.BLACK)
                holder.clockViewWhite.setBackgroundColor(Color.BLACK)
                holder.clockViewRed.setBackgroundColor(Color.BLACK)
                holder.clockViewYellow.setBackgroundColor(Color.BLACK)
            }
            "Red" -> {
                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.red))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.red))
            }
            "Pink" -> {
                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.pink))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.pink))
            }
            "White" -> {

                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.white))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.white))
            }
            "Yellow" -> {
                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.yellow))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.yellow))
            }
            "Orange" -> {
                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.orange))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.orange))
            }
            "Green" -> {
                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.green))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.green))
            }
            "Blue" -> {
                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.blue))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.blue))
            }
            "Grey" -> {

                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.grey))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.grey))
            }
            "Purple" -> {
                holder.clockViewBlack.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewBlue.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewGreen.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewGrey.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewOrange.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewPink.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewPurple.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewWhite.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewRed.setBackgroundColor(context.resources.getColor(R.color.purple))
                holder.clockViewYellow.setBackgroundColor(context.resources.getColor(R.color.purple))
            }
        }
        when (clock.numberColor) {
            "Black" -> {
                holder.clockViewBlack.visibility = View.VISIBLE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.GONE
            }
            "Red" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.VISIBLE
                holder.clockViewYellow.visibility = View.GONE
            }
            "Pink" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.VISIBLE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.GONE
            }
            "White" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.VISIBLE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.GONE
            }
            "Yellow" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.VISIBLE
            }
            "Orange" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.VISIBLE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.GONE
            }
            "Green" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.VISIBLE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.GONE
            }
            "Blue" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.VISIBLE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.GONE
            }
            "Grey" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.VISIBLE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.GONE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.GONE
            }
            "Purple" -> {
                holder.clockViewBlack.visibility = View.GONE
                holder.clockViewBlue.visibility = View.GONE
                holder.clockViewGreen.visibility = View.GONE
                holder.clockViewGrey.visibility = View.GONE
                holder.clockViewOrange.visibility = View.GONE
                holder.clockViewPink.visibility = View.GONE
                holder.clockViewPurple.visibility = View.VISIBLE
                holder.clockViewWhite.visibility = View.GONE
                holder.clockViewRed.visibility = View.GONE
                holder.clockViewYellow.visibility = View.GONE
            }
        }
        when (clock.cityName) {
            "Istanbul" -> {
                chooseCity("Europe", "Istanbul", holder)
                holder.cityNameTxt.text = "Istanbul"
                holder.timeOffsetTxt.text = "0"
            }
            "Amsterdam" -> {
                chooseCity("Europe", "Amsterdam", holder)
                holder.cityNameTxt.text = "Amsterdam"
                holder.timeOffsetTxt.text = "-2"
            }
            "Sofia" -> {
                chooseCity("Europe", "Sofia", holder)
                holder.cityNameTxt.text = "Sofia"
                holder.timeOffsetTxt.text = "-1"
            }
            "Berlin" -> {
                chooseCity("Europe", "Berlin", holder)
                holder.cityNameTxt.text = "Berlin"
                holder.timeOffsetTxt.text = "-2"
            }
            "Los Angeles" -> {
                chooseCity("America", "Los_Angeles", holder)
                holder.cityNameTxt.text = "Los Angeles"
                holder.timeOffsetTxt.text = "-11"
            }
            "New York" -> {
                chooseCity("America", "New_York", holder)
                holder.cityNameTxt.text = "New York"
                holder.timeOffsetTxt.text = "-8"
            }
            "Paris" -> {
                chooseCity("Europe", "Paris", holder)
                holder.cityNameTxt.text = "Paris"
                holder.timeOffsetTxt.text = "-2"
            }
            "Tokyo" -> {
                chooseCity("Asia", "Tokyo", holder)
                holder.cityNameTxt.text = "Tokyo"
                holder.timeOffsetTxt.text = "+6"
            }
            "Dubai" -> {
                chooseCity("Asia", "Dubai", holder)
                holder.cityNameTxt.text = "Dubai"
                holder.timeOffsetTxt.text = "+1"
            }
            "Baku" -> {
                chooseCity("Asia", "Baku", holder)
                holder.cityNameTxt.text = "Baku"
                holder.timeOffsetTxt.text = "+1"
            }
        }


        holder.itemView.setOnLongClickListener {

            Toast.makeText(context, "tıkladın!", Toast.LENGTH_SHORT).show()

            holder.clockListForDelete.addAll(clockDetailList)
            holder.clockListForDelete.remove(clock)
            clockDetailList.removeAt(position)
            notifyItemRemoved(position)

            var clockListForDeleteString = Gson().toJson(holder.clockListForDelete)

            holder.editor.putString("clockList", clockListForDeleteString)

            holder.editor.apply()

            true
        }

    }


    private fun chooseCity(area: String, location: String, holder: ViewHolder) {

        holder.mainHandler.post(object : Runnable {
            override fun run() {
                val call = ApiClient.getClient.getCitys(area, location)
                call.enqueue(object : Callback<String> {
                    override fun onFailure(call: Call<String>?, t: Throwable?) {
                        Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<String>?, response: Response<String>?) =
                        if (response!!.isSuccessful) {
                            //Veriler String biçiminde geldiği için model oluşturulduğunda,expected object found string  hatası verdi.
                            val datetime = response.body().toString()
                            val firstSeparated: List<String> = datetime!!.split("2020")
                            val secondSeparated: List<String> = firstSeparated[1].split("T")
                            val thirdSeparated: List<String> = secondSeparated[1].split(".")
                            val fourthSeparated: List<String> = thirdSeparated[0].split(":")

                            if (fourthSeparated[0].toInt() > 12) {
                                val convertToAnalogTime = fourthSeparated[0].toInt() - 12
                                holder.clockViewYellow.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewRed.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewWhite.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewPurple.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewPink.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewOrange.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewGrey.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewGreen.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewBlue.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewBlack.setTime(
                                    convertToAnalogTime,
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                            } else {
                                holder.clockViewYellow.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewBlack.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewBlue.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewGreen.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewGrey.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewOrange.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewPink.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewPurple.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewWhite.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewRed.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                                holder.clockViewYellow.setTime(
                                    fourthSeparated[0].toInt(),
                                    fourthSeparated[1].toInt(),
                                    fourthSeparated[2].toInt()
                                )
                            }


                        } else {
                            Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT)
                                .show()
                        }
                }
                )
                //5sn'de bir veriler çekilmek istendiğinde response succesful oladığı için dakikada bir olarak değiştirdim.
                holder.mainHandler.postDelayed(this, 60000)
            }
        })


    }
}
