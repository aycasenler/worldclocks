package com.example.ayca.worldclocks

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.worldclocks.adapter.ClockListAdapter
import com.google.gson.Gson
import java.time.Clock
import kotlin.concurrent.fixedRateTimer


class HomeScreenFragment : Fragment(), View.OnClickListener {
    lateinit var recyclerView: RecyclerView
    lateinit var clockDetailList: MutableList<ClockDetail>
    lateinit var adapter: ClockListAdapter
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var emptyList: MutableList<ClockDetail> = mutableListOf()
    lateinit var addClockBtn : ImageButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_screen_fragment, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {
        recyclerView = view.findViewById(R.id.recycler_view)
        clockDetailList = mutableListOf()
        preferences = view.context.getSharedPreferences("clockList", Context.MODE_PRIVATE)
        addClockBtn = view.findViewById(R.id.add_clock_btn)
        getClockDetail()
        setClockListAdapter()
        addClockBtn.setOnClickListener(this)
    }

    private fun setClockListAdapter() {
        recyclerView.layoutManager = GridLayoutManager(context,2)
        adapter = ClockListAdapter(clockDetailList, context!!)
        recyclerView.adapter = adapter
        recyclerView.adapter!!.notifyDataSetChanged()
    }

    @SuppressLint("CommitPrefEdits")
    private fun getClockDetail() {
        if (preferences.getString("clockList", null) == null) {

            editor = preferences.edit()
            editor.putString("clockList", Gson().toJson(emptyList))
            editor.apply()
        }
        clockDetailList.addAll(Gson().fromJson(preferences.getString("clockList",null),Array<ClockDetail>::class.java))
    }

    data class ClockDetail(
        var areaName : String,
        var cityName: String,
        var backgroundColor: String,
        var numberColor: String
    )

    override fun onClick(v: View) {
        Navigation.findNavController(v).navigate(R.id.action_homeScreenFragment_to_createNewClockFragment)
    }
}
