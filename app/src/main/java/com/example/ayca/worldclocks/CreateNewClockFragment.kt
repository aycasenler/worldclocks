package com.example.ayca.worldclocks

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color.BLACK
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.ayca.worldclocks.model.MainData
import com.example.ayca.worldclocks.retrofit.ApiClient
import com.google.gson.Gson
import com.iigo.library.ClockView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("DEPRECATION")
class CreateNewClockFragment : Fragment(), View.OnClickListener {
    lateinit var cityNameList: MutableList<String>
    lateinit var cityNameWithoutNumberList: MutableList<String>
    lateinit var backgroundColorList: List<String>
    lateinit var numberColorList: List<String>
    lateinit var spinnerCityName: Spinner
    lateinit var spinnerClockBackgroundColor: Spinner
    lateinit var spinnerClockNumberColor: Spinner
    lateinit var clockViewBlack: ClockView
    lateinit var clockViewWhite: ClockView
    lateinit var clockViewPink: ClockView
    lateinit var clockViewYellow: ClockView
    lateinit var clockViewBlue: ClockView
    lateinit var clockViewRed: ClockView
    lateinit var clockViewGrey: ClockView
    lateinit var clockViewOrange: ClockView
    lateinit var clockViewPurple: ClockView
    lateinit var clockViewGreen: ClockView
    lateinit var okBtn: ImageButton
    lateinit var cancelBtn: ImageButton
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var clockList: MutableList<HomeScreenFragment.ClockDetail>
    lateinit var mainDataList: MutableList<MainData>
    var clockDetail: HomeScreenFragment.ClockDetail = HomeScreenFragment.ClockDetail("", "", "", "")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.create_new_clock_fragment, container, false)
        initView(view)
        val clockString = preferences.getString("clockList", null)
        val tempClockList = Gson().fromJson(
            clockString,
            Array<HomeScreenFragment.ClockDetail>::class.java
        )

        fillList()
        for (i in 0..cityNameWithoutNumberList.size) {
            if(cityNameWithoutNumberList.size > i)
            for (h in tempClockList) {
                if (h.cityName == cityNameWithoutNumberList[i]) {
                    cityNameWithoutNumberList.removeAt(i)
                    cityNameList.removeAt(i)
                }
            }
        }
        fillSpinners()

        return view
    }

    private fun fillList() {
        cityNameWithoutNumberList = mutableListOf(
            "City Name(Time Offset)",
            "Istanbul",
            "Amsterdam",
            "Sofia",
            "Berlin",
            "Los_Angeles",
            "New_York",
            "Paris",
            "Tokyo",
            "Dubai",
            "Baku"
        )
        cityNameList = mutableListOf(
            "City Name(Time Offset)",
            "Istanbul(0)",
            "Amsterdam(-2)",
            "Sofia(-1)",
            "Berlin(-2)",
            "Los Angeles(-11)",
            "New York(-8)",
            "Paris(-2)",
            "Tokyo(+6)",
            "Dubai(+1)",
            "Baku(+1)"
        )
        backgroundColorList = listOf(
            "Clock Background Color", "Black", "Red", "Pink", "White", "Yellow", "Orange", "Green",
            "Blue", "Grey", "Purple"
        )
        numberColorList = listOf(
            "Clock Number Timer Color",
            "Black",
            "Red",
            "Pink",
            "White",
            "Yellow",
            "Orange",
            "Green",
            "Blue",
            "Grey",
            "Purple"
        )
    }

    private fun fillSpinners() {

        spinnerCityName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {

                when (cityNameList[position]) {
                    "Istanbul(0)" -> {
                        clockDetail.areaName = "Europe"
                        clockDetail.cityName = "Istanbul"
                        chooseCity("Europe", "Istanbul")
                    }
                    "Amsterdam(-2)" -> {
                        clockDetail.areaName = "Europe"
                        clockDetail.cityName = "Amsterdam"
                        chooseCity("Europe", "Amsterdam")
                    }
                    "Sofia(-1)" -> {
                        clockDetail.areaName = "Europe"
                        clockDetail.cityName = "Sofia"
                        chooseCity("Europe", "Sofia")
                    }
                    "Berlin(-2)" -> {
                        clockDetail.areaName = "Europe"
                        clockDetail.cityName = "Berlin"
                        chooseCity("Europe", "Berlin")
                    }
                    "Los Angeles(-11)" -> {
                        clockDetail.areaName = "America"
                        clockDetail.cityName = "Los_Angeles"
                        chooseCity("America", "Los_Angeles")
                    }
                    "New York(-8)" -> {
                        clockDetail.areaName = "America"
                        clockDetail.cityName = "New_York"
                        chooseCity("America", "New_York")
                    }
                    "Paris(-2)" -> {
                        clockDetail.areaName = "Europe"
                        clockDetail.cityName = "Paris"
                        chooseCity("Europe", "Paris")
                    }
                    "Tokyo(+6)" -> {
                        clockDetail.areaName = "Asia"
                        clockDetail.cityName = "Tokyo"
                        chooseCity("Asia", "Tokyo")
                    }
                    "Dubai(+1)" -> {
                        clockDetail.areaName = "Asia"
                        clockDetail.cityName = "Dubai"
                        chooseCity("Asia", "Dubai")
                    }
                    "Baku(+1)" -> {
                        clockDetail.areaName = "Asia"
                        clockDetail.cityName = "Baku"
                        chooseCity("Asia", "Baku")
                    }
                }
            }

        }
        spinnerClockBackgroundColor.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?, view: View?, position: Int, id: Long
                ) {
                    changeBackgroundColor(position)

                }

            }
        spinnerClockNumberColor.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    changeNumberColor(position)

                }

            }

        val cityNameAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, cityNameList)
        val clockBackgroundAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, backgroundColorList)
        val clocknumberAdapter =
            ArrayAdapter(context!!, android.R.layout.simple_spinner_item, numberColorList)
        cityNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        clockBackgroundAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        clocknumberAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinnerCityName.adapter = cityNameAdapter
        spinnerClockBackgroundColor.adapter = clockBackgroundAdapter
        spinnerClockNumberColor.adapter = clocknumberAdapter

    }

    private fun chooseCity(area: String, location: String) {

        val call = ApiClient.getClient.getCitys(area, location)
        call.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<String>?, response: Response<String>?) =
                if (response!!.isSuccessful) {


//                 //Veriler String biçimde geldiği için model oluşturulduğunda,expected object found string  hatası verdi.


                    val datetime = response.body().toString()
                    val firstSeparated: List<String> = datetime!!.split("2020")
                    val secondSeparated: List<String> = firstSeparated[1].split("T")
                    val thirdSeparated: List<String> = secondSeparated[1].split(".")
                    val fourthSeparated: List<String> = thirdSeparated[0].split(":")
                    if (fourthSeparated[0].toInt() > 12) {
                        val convertToAnalogTime = fourthSeparated[0].toInt() - 12
                        clockViewYellow.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewRed.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewWhite.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewPurple.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewPink.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewOrange.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewGrey.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewGreen.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewBlue.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewBlack.setTime(
                            convertToAnalogTime,
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                    } else {
                        clockViewYellow.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewBlack.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewBlue.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewGreen.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewGrey.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewOrange.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewPink.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewPurple.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewWhite.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewRed.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                        clockViewYellow.setTime(
                            fourthSeparated[0].toInt(),
                            fourthSeparated[1].toInt(),
                            fourthSeparated[2].toInt()
                        )
                    }


                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT)
                        .show()
                }
        }
        )
    }

    private fun changeBackgroundColor(position: Int) {

        when (backgroundColorList[position]) {
            "Black" -> {
                clockDetail.backgroundColor = "Black"
                clockViewBlack.setBackgroundColor(BLACK)
                clockViewBlue.setBackgroundColor(BLACK)
                clockViewGreen.setBackgroundColor(BLACK)
                clockViewGrey.setBackgroundColor(BLACK)
                clockViewOrange.setBackgroundColor(BLACK)
                clockViewPink.setBackgroundColor(BLACK)
                clockViewPurple.setBackgroundColor(BLACK)
                clockViewWhite.setBackgroundColor(BLACK)
                clockViewRed.setBackgroundColor(BLACK)
                clockViewYellow.setBackgroundColor(BLACK)
            }
            "Red" -> {
                clockDetail.backgroundColor = "Red"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.red))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.red))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.red))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.red))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.red))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.red))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.red))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.red))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.red))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.red))
            }
            "Pink" -> {
                clockDetail.backgroundColor = "Pink"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.pink))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.pink))
            }
            "White" -> {
                clockDetail.backgroundColor = "White"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.white))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.white))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.white))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.white))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.white))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.white))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.white))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.white))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.white))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.white))
            }
            "Yellow" -> {
                clockDetail.backgroundColor = "Yellow"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.yellow))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.yellow))
            }
            "Orange" -> {
                clockDetail.backgroundColor = "Orange"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.orange))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.orange))
            }
            "Green" -> {
                clockDetail.backgroundColor = "Green"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.green))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.green))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.green))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.green))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.green))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.green))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.green))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.green))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.green))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.green))
            }
            "Blue" -> {
                clockDetail.backgroundColor = "Blue"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.blue))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.blue))
            }
            "Grey" -> {
                clockDetail.backgroundColor = "Grey"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.grey))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.grey))
            }
            "Purple" -> {
                clockDetail.backgroundColor = "Purple"
                clockViewBlack.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewBlue.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewGreen.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewGrey.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewOrange.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewPink.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewPurple.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewWhite.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewRed.setBackgroundColor(resources.getColor(R.color.purple))
                clockViewYellow.setBackgroundColor(resources.getColor(R.color.purple))
            }
        }
    }

    private fun changeNumberColor(position: Int) {
        when (numberColorList[position]) {
            "Black" -> {
                clockDetail.numberColor = "Black"
                clockViewBlack.visibility = View.VISIBLE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.GONE
            }
            "Red" -> {
                clockDetail.numberColor = "Red"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.VISIBLE
                clockViewYellow.visibility = View.GONE
            }
            "Pink" -> {
                clockDetail.numberColor = "Pink"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.VISIBLE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.GONE
            }
            "White" -> {
                clockDetail.numberColor = "White"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.VISIBLE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.GONE
            }
            "Yellow" -> {
                clockDetail.numberColor = "Yellow"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.VISIBLE
            }
            "Orange" -> {
                clockDetail.numberColor = "Orange"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.VISIBLE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.GONE
            }
            "Green" -> {
                clockDetail.numberColor = "Green"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.VISIBLE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.GONE
            }
            "Blue" -> {
                clockDetail.numberColor = "Blue"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.VISIBLE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.GONE
            }
            "Grey" -> {
                clockDetail.numberColor = "Grey"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.VISIBLE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.GONE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.GONE
            }
            "Purple" -> {
                clockDetail.numberColor = "Purple"
                clockViewBlack.visibility = View.GONE
                clockViewBlue.visibility = View.GONE
                clockViewGreen.visibility = View.GONE
                clockViewGrey.visibility = View.GONE
                clockViewOrange.visibility = View.GONE
                clockViewPink.visibility = View.GONE
                clockViewPurple.visibility = View.VISIBLE
                clockViewWhite.visibility = View.GONE
                clockViewRed.visibility = View.GONE
                clockViewYellow.visibility = View.GONE
            }
        }
    }

    private fun initView(view: View) {
        cityNameList = mutableListOf()
        cityNameWithoutNumberList = mutableListOf()
        spinnerCityName = view.findViewById(R.id.spinner_city_name)
        spinnerClockBackgroundColor = view.findViewById(R.id.spinner_clock_background_color)
        spinnerClockNumberColor = view.findViewById(R.id.spinner_clock_number_color)
        clockViewBlack = view.findViewById(R.id.clockViewBlack)
        clockViewBlue = view.findViewById(R.id.clockViewBlue)
        clockViewGreen = view.findViewById(R.id.clockViewGreen)
        clockViewGrey = view.findViewById(R.id.clockViewGrey)
        clockViewOrange = view.findViewById(R.id.clockViewOrange)
        clockViewPink = view.findViewById(R.id.clockViewPink)
        clockViewPurple = view.findViewById(R.id.clockViewPurple)
        clockViewWhite = view.findViewById(R.id.clockViewWhite)
        clockViewRed = view.findViewById(R.id.clockViewRed)
        clockViewYellow = view.findViewById(R.id.clockViewYellow)
        okBtn = view.findViewById(R.id.ok_btn)
        cancelBtn = view.findViewById(R.id.cancel_btn)
        clockList = mutableListOf()
        mainDataList = mutableListOf()
        preferences =
            view.context.getSharedPreferences("clockList", Context.MODE_PRIVATE)
        editor = preferences.edit()
        okBtn.setOnClickListener(this)
        cancelBtn.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            okBtn.id -> {

                if (spinnerCityName.selectedItem != "City Name(Time Offset)") {
                    if (spinnerClockBackgroundColor.selectedItem != "Clock Background Color") {
                        if (spinnerClockNumberColor.selectedItem != "Clock Number Timer Color") {

                            clockList = mutableListOf()

                            if (!preferences.getString("clockList", null)!!.isNullOrEmpty()) {
                                val tempList = preferences.getString("clockList", null)

                                clockList.addAll(
                                    Gson().fromJson(
                                        tempList,
                                        Array<HomeScreenFragment.ClockDetail>::class.java
                                    )
                                )
                                clockList.add(clockDetail)
                                editor.putString("clockList", Gson().toJson(clockList))
                                editor.apply()
                            } else {
                                clockList.add(clockDetail)
                                editor.putString("clockList", Gson().toJson(clockList))
                                editor.apply()
                            }
                            Navigation.findNavController(v).popBackStack()
                        } else {
                            Toast.makeText(
                                context,
                                "Please Select All Options!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(context, "Please Select All Options!", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {
                    Toast.makeText(context, "Please Select All Options!", Toast.LENGTH_SHORT).show()
                }


            }
            cancelBtn.id -> {
                Navigation.findNavController(v).popBackStack()
            }


        }
    }

}


