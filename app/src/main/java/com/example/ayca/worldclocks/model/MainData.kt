package com.example.ayca.worldclocks.model

import com.google.gson.annotations.SerializedName

data class MainData(
    @SerializedName("datetime")
    val datetime : String
)